package test.java.com.sni.home.CQDataTool;


public class CleanImageMeta {

    public String cleanImageMeta(String retVal) throws Exception
    {
        String cleanStr="";
        String copyPattern = "&copy;";
        String copyHex = "&#169;";
        String dQuotePattern = "&quot;";
        String dQuoteHex = "&#34;";
        String sQuoteHex = "&#39;";
        String ampPattern = "&amp;";
        String ampHex = "&#38;";
        String spacePattern = "&nbsp;";
        String spaceHex = "&#160;";
        String AcircPattern = "&Acirc;";
        String AcircHex = "&#194;";

        cleanStr=retVal.toString();
        //strip some chars
        cleanStr = cleanStr.replaceAll(copyPattern, "")
                .replaceAll(copyHex, "")
                .replaceAll(AcircPattern, "")
                .replaceAll(AcircHex, "");

        //decode other chars
        cleanStr = cleanStr.replaceAll(dQuotePattern, "\"")
                .replaceAll(dQuoteHex, "\"")
                .replaceAll(sQuoteHex, "\'")
                .replaceAll(ampPattern, "&")
                .replaceAll(ampHex, "&")
                .replaceAll(spacePattern, " ")
                .replaceAll(spaceHex, " ");

        return cleanStr;
    }

}
