package test.java.com.sni.home.CQDataTool.models;

import test.java.com.sni.home.CQDataTool.Util;
import test.java.com.sni.home.CQDataTool.config.CQConfig;
import test.java.com.sni.home.CQDataTool.interfaces.Connection;

import org.apache.jackrabbit.commons.JcrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

/**
 * Created with IntelliJ IDEA.
 * User: Tripp Shewmake
 * Date: 6/10/14
 * Time: 9:13 PM
 * Returns a recipe template based on a type requested
 */
public class CQConnectionImpl implements Connection{

    static Logger log = LoggerFactory.getLogger(CQConnectionImpl.class);
    private CQConfig cqConfig;
    private Repository repository;
    private Session session;

    public void login() throws Exception{
        log.info("Logging in to instance");
        try {
            session = repository.login(new SimpleCredentials(cqConfig.getUser(), cqConfig.getPassword().toCharArray()));

        } catch (Exception e) {
            log.error(Util.formatExceptionMessage("Error logging into instance", e));
            throw e;
        }
        log.info ("Login successful on " + cqConfig.getHost());
    }

    public void logout() {
        try {
        	session.save();
        	log.info("Session has been saved");
            } catch (Exception e)
          {
            log.info("Logging out of " + cqConfig.getHost());
            session.logout();
          }
        session.logout();
    }

    public CQConnectionImpl() throws Exception {
        log.info("Get Spring application context");
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        log.info("Load CQ configuration values");
        this.cqConfig = (CQConfig) context.getBean("CQConfig");
        try {
            log.info("CQ Host: " + cqConfig.getHost());
            log.info("CQ User: " + cqConfig.getUser());
            log.info("CQ Password: " + cqConfig.getPassword());
        } catch (Exception e) {
            log.error("CQ configuration failed to load");
            context.close();
            throw e;
            
        }
        context.close();

        String repoUrl = cqConfig.getHost() + cqConfig.getPath();
        log.info("Configuring repository " + repoUrl);

        try {
            this.repository = JcrUtils.getRepository(repoUrl);

        } catch (RepositoryException re) {
            log.error(Util.formatExceptionMessage("Repository Exception", re));
            throw re;
        } catch (Exception e) {
            log.error(Util.formatExceptionMessage("Unknown Exception",e));
            throw e;
        }
    }
}
