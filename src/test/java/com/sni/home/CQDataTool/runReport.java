package test.java.com.sni.home.CQDataTool;

import java.io.FileWriter;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class runReport {

    private static final Logger log =  LoggerFactory.getLogger(UpdateImageMeta.class.getSimpleName());

    private Node rootNode;

    public runReport(Node node)
    {
        this.rootNode = node;
    }

    public void runImageReport() throws Exception {

        try {
            FileWriter fw = new FileWriter(Util.outputfile);
            // Obtain the query manager for the session via the workspace ...
            QueryManager queryManager = rootNode.getSession().getWorkspace().getQueryManager();


            String[] buckets = { "fullset", "unsized" };
            String[] years = { 	"2002", "2003", "2004", "2005", "2006", "2007",
                    "2008", "2009", "2010", "2011", "2012", "2013", "2014" };

            String propvalue = "";
            String l_Field = "";
            String colHeaders = "CQPath>CleanedDCCopyright>CleanedSNICopyright>CleanedSNIDCCopyright"+ "\n";
            String outPath = "";
            String outDCCopyright = "";
            String outSNICopyright = "";
            String outSNIDCCopyright = "";
            String delimiter=Util.DELIMITER;


            fw.append(colHeaders);
            CleanImageMeta cleanIMeta = new CleanImageMeta();
            for (String year : years)
            {
                System.out.printf("%s:\n", year);
                for (String bucket : buckets)
                {
                    String queryString = Util.Queries.IMAGES_SQL2.query().replaceAll("BUCKET", bucket);
                    queryString = queryString.replaceAll("YEAR", year);
                    Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);

                    // Execute the query and get the results ...
                    javax.jcr.query.QueryResult result = query.execute();
                    RowIterator rows = result.getRows();
                    while (rows.hasNext())
                    {
                        outPath = "";
                        outDCCopyright = "";
                        outSNICopyright = "";
                        outSNIDCCopyright = "";
                        Row row = rows.nextRow();
                        Node n = row.getNode();
                        outPath = n.getPath();
                        PropertyIterator pi = n.getProperties();
                        while (pi.hasNext())
                        {

                            Property p = pi.nextProperty();
                            l_Field = p.getName();

                            switch (l_Field) {

                                case "dc:copyright":
                                    if (p.getDefinition().isMultiple()) {
                                        Value[] values = p.getValues();
                                        for (int i = 0; i < values.length; i++) {
                                            propvalue = values[i].getString();
                                            outDCCopyright = cleanIMeta.cleanImageMeta(propvalue);
                                        }// End of values for loop
                                    } else {
                                        propvalue = p.getValue().getString();
                                        outDCCopyright = cleanIMeta.cleanImageMeta(propvalue);
                                    }// End if isMultiple
                                    break;

                                case "sni:copyright":
                                    if (p.getDefinition().isMultiple()) {
                                        Value[] values = p.getValues();
                                        for (int i = 0; i < values.length; i++) {
                                            propvalue = values[i].getString();
                                            outSNICopyright = cleanIMeta.cleanImageMeta(propvalue);
                                        }// End of values for loop
                                    } else {
                                        propvalue = p.getValue().getString();
                                        outSNICopyright = cleanIMeta.cleanImageMeta(propvalue);
                                    }// End if isMultiple
                                    break;
                                case "sni:dc-copyright":
                                    if (p.getDefinition().isMultiple()) {
                                        Value[] values = p.getValues();
                                        for (int i = 0; i < values.length; i++) {
                                            propvalue = values[i].getString();
                                            outSNIDCCopyright = cleanIMeta.cleanImageMeta(propvalue);
                                        }// End of values for loop
                                    } else {
                                        propvalue = p.getValue().getString();
                                        outSNIDCCopyright =cleanIMeta.cleanImageMeta(propvalue);
                                    }// End if isMultiple
                                    break;

                                default:
                                    break;
                            }// End switch statement

                        }// End While statement
                        System.out.println(outPath + delimiter + outDCCopyright + delimiter+ outSNICopyright + delimiter + outSNIDCCopyright+ "\n");
                        fw.append(outPath + delimiter + outDCCopyright + delimiter+ outSNICopyright + delimiter + outSNIDCCopyright + "\n");
                    }//End While Loop

                }//End for loop buckets
            }//End for loop years

            fw.close();
        }

        catch (Exception e)
        {
            log.error(Util.formatExceptionMessage("Run Report Exception", e));
        }

    }//End runImageReport

}//End runReport
