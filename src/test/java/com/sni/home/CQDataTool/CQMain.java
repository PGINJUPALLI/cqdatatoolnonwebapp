package test.java.com.sni.home.CQDataTool;

import test.java.com.sni.home.CQDataTool.interfaces.Connection;
import test.java.com.sni.home.CQDataTool.models.CQConnectionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pavan Ginjupalli
 *         Date: 05/29/14
 *         Tripp Shewmake
 *         Date: 06/10/2014
 */
public class CQMain {

    static Logger log = LoggerFactory.getLogger(CQMain.class);

    /**
     * Main
     * args can be any of LOCALHOST, DEV etc (values are in HostSettings enum),
     - for update second argument is UPDATE
     - for report second argument is REPORT
     * this will load settings for that environment. LOCALHOST is default if no input.
     * @param args Optional environment to run against LOCALHOST, DEV etc
     */

    public static void main(final String[] args) {

        log.info("starting application");
        Connection connection = null;
        try {
            connection = new CQConnectionImpl();
            connection.login();
         
                log.info("in Report mode");
                runReport imageReport = new runReport(Connection.repository);
                imageReport.runImageReport();
  

        connection.logout();
        log.info("Application exiting under normal conditions");
        System.exit(0);
        }
        catch (Exception e)
        {
            log.error(Util.formatExceptionMessage("Exception in Main", e));
            System.exit(1);
        }
    }//End of Main
}//End of class CQMain



