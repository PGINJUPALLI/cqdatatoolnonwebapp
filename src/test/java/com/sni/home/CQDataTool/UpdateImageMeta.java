package test.java.com.sni.home.CQDataTool;

import java.io.BufferedReader;
import java.io.FileReader;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * @author Pavan Ginjupalli Date: 06/01/14
 */
public class UpdateImageMeta {

    private static final Logger log = LoggerFactory.getLogger(CQMain.class.getSimpleName());

    private Node rootNode;

    public UpdateImageMeta(Node node)
    {
        this.rootNode = node;
    }

    public void UpdateImageProperty() throws Exception {

        try {
            FileReader input = new FileReader(Util.inputfile);
            BufferedReader bufRead = new BufferedReader(input);
            String myLine = null;

            while ((myLine = bufRead.readLine()) != null)
            {
                String[] param = myLine.split(Util.DELIMITER,-1);
                for(int i=0;i<param.length;i++)
                {
                    log.info("Arg "+i+" is :"+param[i].toString()+"\n");
                }

                String imgNodeStr 	= param[0].toString();
                String dccpyrght 	= param[1].toString();
                String snicpyrght 	= param[2].toString();
                String snidccpyrght = param[3].toString();

                Node imgNode = rootNode.getSession().getNode(imgNodeStr);
                log.info("Image Node is:" + imgNode);
                PropertyIterator propIterator = imgNode.getProperties();
                while (propIterator.hasNext())
                {
                    Property prop = propIterator.nextProperty();
                    if (!prop.getDefinition().isMultiple()&& prop.getName().equals("dc:copyright")&& !dccpyrght.isEmpty()) {
                        System.out.println(" " + prop.getName() + " = "+ prop.getString() + " >> " + prop.getValue().toString());
                        prop.setValue(dccpyrght);
                    }
                    if (!prop.getDefinition().isMultiple()&& prop.getName().equals("sni:copyright") && !snicpyrght.isEmpty()) {
                        System.out.println(" " + prop.getName() + " = "+ prop.getString() + " >> " + prop.getValue().toString());
                        prop.setValue(snicpyrght);
                    }
                    if (!prop.getDefinition().isMultiple()&& prop.getName().equals("sni:dc-copyright") && !snidccpyrght.isEmpty()) {
                        System.out.println(" " + prop.getName() + " = "+ prop.getString() + " >> " + prop.getValue().toString());
                        prop.setValue(snidccpyrght);
                    }
                }

            }
            bufRead.close();
        }
        catch (Exception e)
        {
            log.error(Util.formatExceptionMessage("Run Update Exception", e));
        }

    }//End UpdateImageProperty Method

}//End class UpdateImageMeta
