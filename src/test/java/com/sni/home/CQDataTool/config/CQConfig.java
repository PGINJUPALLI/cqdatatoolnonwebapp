package test.java.com.sni.home.CQDataTool.config;

import javax.jcr.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: Tripp Shewmake
 * Date: 6/10/14
 * Time: 7:03 PM
 * Returns a recipe template based on a type requested
 */
public class CQConfig {
	
    private String host;
    private String path;
    private String user;
    private String password;
    private Repository repository;
    
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String pass) {
        this.password = pass;
    }

	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}


}
