package test.java.com.sni.home.CQDataTool.interfaces;

import javax.jcr.Node;

/**
 * Created with IntelliJ IDEA.
 * User: Tripp Shewmake
 * Date: 6/10/14
 * Time: 9:12 PM
 * Returns a recipe template based on a type requested
 */
public interface Connection {

    public Node repository = null;
	public void login() throws Exception;
    public void logout();
}
