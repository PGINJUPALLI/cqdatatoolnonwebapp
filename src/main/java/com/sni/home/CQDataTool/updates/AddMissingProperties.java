package com.sni.home.CQDataTool.updates;


	

	import java.io.BufferedReader;
import java.io.FileReader;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

	/**
	 * @author Pavan Ginjupalli Date: 06/01/14
	 */
public class AddMissingProperties {

	    private static final Logger log = LoggerFactory.getLogger(UpdateImageMeta.class);

	    public static void addProperty(CQDataConfig cfg) throws Exception {
	    	
	    	

	        try {
	        	System.out.println("The Input update file name is: "+cfg.getInfile());
	            FileReader input = new FileReader(cfg.getInfile());
	            BufferedReader bufRead = new BufferedReader(input);
	            String myLine = null;
	            int saverecordno=0;
	            int bufrecordno=0;
	            

	            while ((myLine = bufRead.readLine()) != null)
	            {
	                String[] param = myLine.split(cfg.getDelimit(),-1);
	                for(int i=0;i<param.length;i++)
	                {
	                   // log.info("Arg "+i+" is :"+param[i].toString()+"\n");
	                }

	                String cqNodeStr 		= param[0].toString();
	                String copyright 		= param[1].toString();
	                String countertimecode 	= param[2].toString();
	                
	                
	                
	                Node cqNode = CQDataConfig.getRootNode().getSession().getNode(cqNodeStr);
	                
	                System.out.println("\n"+saverecordno+"/"+bufrecordno+": Node String getting updated is:"+cqNode.getPath());
	                
	                PropertyIterator propIterator = cqNode.getProperties();
	                while (propIterator.hasNext())
	                {
	                    Property prop = propIterator.nextProperty();
	                    if (prop.getDefinition().isMultiple()&& prop.getName().equals("dc:copyright")&& !copyright.isEmpty()) 
	                    {	                        
	                    	  
    	                         
    	                          try{
    	                          cqNode.setProperty(prop.getName(),copyright);  
    	                          }
    	                          catch(Exception e){
    	                        	  System.out.println("Your exception is:"+e);
    	                          }
    	                                 
	                    }
	                    
	                    if (prop.getDefinition().isMultiple()&& prop.getName().equals("sni:dc-copyright")&& !copyright.isEmpty()) 
	                    {	                        
	                    	  
    	                          try{
    	                          cqNode.setProperty(prop.getName(),copyright);  
    	                          }
    	                          catch(Exception e){
    	                        	  System.out.println("Your exception is:"+e);
    	                          }
    	                                 
	                    }
	                    
	                    if (prop.getDefinition().isMultiple()&& prop.getName().equals("sni:counterTimeCode")&& !countertimecode.isEmpty()) 
	                    {	                        
	                    	  
    	                          try{
    	                          cqNode.setProperty(prop.getName(),countertimecode);  
    	                          }
    	                          catch(Exception e){
    	                        	  System.out.println("Your exception is:"+e);
    	                          }
    	                                 
	                    }
	                    
	                    
	                    if(!copyright.isEmpty() && !propertyExists(cqNode,"dc:copyright"))
	                    {
	                    		                    	
	                    	  try{
    	                          cqNode.setProperty("dc:copyright",copyright);  
    	                          }
    	                          catch(Exception e){
    	                        	  System.out.println("Your exception is:"+e);
    	                          }
	                    	
	                    }
	                    if(!copyright.isEmpty() && !propertyExists(cqNode,"sni:dc-copyright"))
	                    {
	                    		                    	
	                    	  try{
    	                          cqNode.setProperty("sni:dc-copyright",copyright);  
    	                          }
    	                          catch(Exception e){
    	                        	  System.out.println("Your exception is:"+e);
    	                          }
	                    	
	                    }
	                    if(!countertimecode.isEmpty() && !propertyExists(cqNode,"sni:counterTimeCode"))
	                    {
	                    		                    	
	                    	  try{
    	                          cqNode.setProperty("sni:counterTimeCode",countertimecode);  
    	                          }
    	                          catch(Exception e){
    	                        	  System.out.println("Your exception is:"+e);
    	                          }
	                    	
	                    }
	                  
	                    

	                   
	                    
	                }// end while loop
	                
	   
	                
	                if(saverecordno>50)
	                {
	                	saverecordno=0;
	                	CQDataConfig.getRootNode().getSession().save();
	                	log.info("*********** Saving the updates **********************");
	                }else{
	                	saverecordno=saverecordno+1;
	                }
	                
	                bufrecordno=bufrecordno+1;

	            }
	            bufRead.close();
	        }
	        catch (Exception e)
	        {
	            log.error(ExceptionMsg.formatExceptionMessage("Run Update Exception", e));
	        }

	    }//End adkey Method
	    
	    public static boolean propertyExists(Node cqNode,String propname) 

	    {
	    	 try { 
	    		 
		    		 PropertyIterator propIterator = cqNode.getProperties();
		    		 
		    		 int flag=0;
		    		 
		    		 while (propIterator.hasNext())
		    		 {
	                 
		    			 Property prop = propIterator.nextProperty();
	                
		    			 if(prop.getName().equals(propname))
		    			 {
							flag=1;
						 }			 
		    			 
						 
		    		 }
		    		 
		    		 if(flag==1)
		    		 {
		    			 return true;
		    		 }
		    		 else
		    		 {
		    			 return false;
		    		 }
					
	    	 	} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				} 
                 
            
	    	
	    }//End propertyExists Method


}
