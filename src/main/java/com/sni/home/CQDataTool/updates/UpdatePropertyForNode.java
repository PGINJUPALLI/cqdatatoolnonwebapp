package com.sni.home.CQDataTool.updates;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.lock.LockException;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/*
 * @author Pavan Ginjupalli Date: 06/01/14
 
 ** Example Input
 	/content/hgtv-com/en/jcr:content/assetTitle|true|true
	/content/hgtv-com/en/jcr:content/assetDescription|true|true
	/content/hgtv-com/en/about-us/hgtv-newsletters/jcr:content/assetTitle|true|true
	/content/hgtv-com/en/about-us/hgtv-on-the-go/jcr:content/assetTitle|true|true
 
 ** Property file
 
     	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("property")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   UpdatePropertyForNode.updateProperty(cqdataconfobj);
    	   }
           
           
 ** Bean Config
     <bean id="Update" class="com.sni.home.CQDataTool.config.CQDataConfig">
        <property name="infile" value="C:/files/outputfiles/input.txt" />
        <property name="delimit" value="\|" />
        <property name="updatetype" value="property" />
        <property name="fields">
            <list>
            <value>hideInMobile</value>
            <value>hideInDesktop</value>            
            </list>
        </property>
    </bean>
 */
public class UpdatePropertyForNode {

	private static final Logger log = LoggerFactory.getLogger(UpdatePropertyForNode.class);

	public static void updateProperty(CQDataConfig cfg) throws Exception {



		try {
			System.out.println("The Input update file name is: " + cfg.getInfile());
			FileReader input = new FileReader(cfg.getInfile());
			BufferedReader bufRead = new BufferedReader(input);
			String myLine = null;
			int saverecordno = 0;
			int bufrecordno = 0;
			Node cqNode = null;
			String[] param = null;



			while ((myLine = bufRead.readLine()) != null) {
				param = myLine.split(cfg.getDelimit(), -1);
				for (int i = 0; i < param.length; i++) {
					// log.info("Arg "+i+" is :"+param[i].toString()+"\n");
				}

				String cqNodeStr = param[0].toString();
				int item = 1;

				try {
					cqNode = CQDataConfig.getRootNode().getSession().getNode(cqNodeStr);

				} catch (Exception notanode) {
					System.out.println(cqNodeStr + " does not exist..skipping to next");
				}

				System.out.println("\n" + saverecordno + "/" + bufrecordno + ": Node String getting updated is:" + cqNodeStr);

				for (String lfield: cfg.getFields()) {

					PropertyIterator propIterator = cqNode.getProperties();

					while (propIterator.hasNext()) {
						Property prop = propIterator.nextProperty();

						if (prop.getName().equals(lfield) && !param[item].toString().isEmpty()) {
							System.out.println("Property getname:" + prop.getName() + " assigning a value of:" + param[item].toString());
							try {
								cqNode.setProperty(prop.getName(), param[item].toString());							
							}
							catch (Exception e) {
								System.out.println("Your exception is:" + e);
							}

						}

						if (!param[item].toString().isEmpty() && !propertyExists(cqNode, lfield)) {

							try {
								System.out.println("Property getname:"+ lfield + " doesnt exist so adding it and assigning a value of "+param[item].toString());
								cqNode.setProperty(lfield, param[item].toString());
							} 
							catch (Exception e) {
								System.out.println("Your exception is:" + e);
							}

						}




					} // end while loop
					item++;

				} // end fields



				if (saverecordno > 10) {
					saverecordno = 0;
					CQDataConfig.getRootNode().getSession().save();
					log.info("*********** Saving the updates **********************");
				} else {
					saverecordno = saverecordno + 1;
				}

				bufrecordno = bufrecordno + 1;

			}
			param = null;
			CQDataConfig.getRootNode().getSession().save();
			bufRead.close();
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage("Run Update Exception", e));
		}

	}

	public static boolean propertyExists(Node cqNode, String propname)

	{
		try {

			PropertyIterator propIterator = cqNode.getProperties();

			int flag = 0;

			while (propIterator.hasNext()) {

				Property prop = propIterator.nextProperty();

				if (prop.getName().equals(propname)) {
					flag = 1;
				}


			}

			if (flag == 1) {
				return true;
			} else {
				return false;
			}

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}



	} //End propertyExists Method



}