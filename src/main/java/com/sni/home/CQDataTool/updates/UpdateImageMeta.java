package com.sni.home.CQDataTool.updates;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.CleanImageMeta;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * @author Pavan Ginjupalli Date: 06/01/14
 */
public class UpdateImageMeta {

    private static final Logger log = LoggerFactory.getLogger(UpdateImageMeta.class);

    public static void UpdateImageProperty(CQDataConfig cfg) throws Exception {
    	
    	

        try {
        	System.out.println("The Input update file name is: "+cfg.getInfile());
            FileReader input = new FileReader(cfg.getInfile());
            BufferedReader bufRead = new BufferedReader(input);
            CleanImageMeta clean = new CleanImageMeta();
            String myLine = null;
            int saverecordno=0;
            int bufrecordno=0;

            while ((myLine = bufRead.readLine()) != null)
            {
                String[] param = myLine.split(cfg.getDelimit(),-1);
                for(int i=0;i<param.length;i++)
                {
                    //log.info("Arg "+i+" is :"+param[i].toString()+"\n");
                }

                String imgNodeStr 	= param[0].toString();
                String dccpyrght 	= param[1].toString();
                String snicpyrght 	= param[2].toString();
                String snidccpyrght = param[3].toString();
                
                System.out.println("\n"+saverecordno+"/"+bufrecordno+":Image Node String getting updated is:"+imgNodeStr);
                
                Node imgNode = CQDataConfig.getRootNode().getSession().getNode(imgNodeStr);
                //log.info("Image Node is:" + imgNode);
                PropertyIterator propIterator = imgNode.getProperties();
                while (propIterator.hasNext())
                {
                    Property prop = propIterator.nextProperty();
                    if (!prop.getDefinition().isMultiple()&& prop.getName().equals("dc:copyright")&& !dccpyrght.isEmpty()) {
                        //System.out.println(" " + prop.getName() + " = "+ prop.getString() + " >> " + prop.getValue().toString());
                        System.out.println("DC Copyright:Before-->"+dccpyrght);
                        System.out.println("DC Copyright:After-->"+clean.cleanImageMeta(dccpyrght));
                        prop.setValue(clean.cleanImageMeta(dccpyrght));
                        //prop.setValue(dccpyrght);
                    }
                    if (!prop.getDefinition().isMultiple()&& prop.getName().equals("sni:copyright") && !snicpyrght.isEmpty()) {
                        //System.out.println(" " + prop.getName() + " = "+ prop.getString() + " >> " + prop.getValue().toString());
                    	System.out.println("SNI Copyright:Before-->"+snicpyrght);
                    	System.out.println("SNI Copyright:After-->"+clean.cleanImageMeta(snicpyrght));
                        prop.setValue(clean.cleanImageMeta(snicpyrght));
                    	//prop.setValue(snicpyrght);
                    }
                    if (!prop.getDefinition().isMultiple()&& prop.getName().equals("sni:dc-copyright") && !snidccpyrght.isEmpty()) {
                        //System.out.println(" " + prop.getName() + " = "+ prop.getString() + " >> " + prop.getValue().toString());
                    	System.out.println("SNI DC Copyright:Before-->"+snidccpyrght);
                    	System.out.println("SNI DC Copyright:After-->"+clean.cleanImageMeta(snidccpyrght));
                        prop.setValue(clean.cleanImageMeta(snidccpyrght));
                    	//prop.setValue(snidccpyrght);
                    }
                }
                
                if(saverecordno>500)
                {
                	saverecordno=0;
                	CQDataConfig.getRootNode().getSession().save();
                	log.info("*********** Saving the updates **********************");
                }else{
                	saverecordno=saverecordno+1;
                }
                
                bufrecordno=bufrecordno+1;

            }
            bufRead.close();
        }
        catch (Exception e)
        {
            log.error(ExceptionMsg.formatExceptionMessage("Run Update Exception", e));
        }

    }//End UpdateImageProperty Method

}//End class UpdateImageMeta
