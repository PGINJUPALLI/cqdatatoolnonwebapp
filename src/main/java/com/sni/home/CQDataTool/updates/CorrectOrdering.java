package com.sni.home.CQDataTool.updates;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.GetCQPropertyValue;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/* An example input file content
 * /content/diy-com/en/pavantesting/pavangalleries/jcr:content/photoGalleryPromo
 * 
 * Append following to CQDataUpdateFactory
 *     	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("ordernode")) {
 System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
 CorrectOrdering.rearrange(cqdataconfobj);
 }
 * 
 * Add following to Beanfile
 *     <bean id="Update" class="com.sni.home.CQDataTool.config.CQDataConfig">
 <property name="infile" value="C:/files/outputfiles/input.txt" />
 <property name="updatetype" value="ordernode" />
 </property>
 </bean>

 *Program Argument: update
 */
public class CorrectOrdering {

	private static final Logger log = LoggerFactory.getLogger(CorrectOrdering.class);

	public static void rearrange(CQDataConfig cfg) throws Exception {

		try {
			/* Initialization of all variables */
			System.out.println("The Input update file name is: " + cfg.getInfile());
			FileReader input = new FileReader(cfg.getInfile());
			BufferedReader bufRead = new BufferedReader(input);
			String myLine = null;
			Node cqNode = null;
			String itemname = null;
			int keepflag = 1;
			Set < Node > itemnodes = new LinkedHashSet < Node > (); // converted to LinkedHashSetto preserve order
			Set < String > items = new LinkedHashSet < String > ();
			Map < Integer, Node > mapnodes = new TreeMap < Integer, Node > (); //Fix:Using treemap to sort elements
			GetCQPropertyValue gcqpv = new GetCQPropertyValue();
			ArrayList < Node > dupnodes = new ArrayList < Node > ();




			/* Read the input file lines one by one */
			while ((myLine = bufRead.readLine()) != null) {
				String[] param = myLine.split(cfg.getDelimit(), -1);
				String cqNodeStr = param[0].toString();
				String extension = cqNodeStr.substring(cqNodeStr.lastIndexOf("/") + 1);


				/*Only process nodes referred by extension */
				if (extension.equalsIgnoreCase("photoGalleryPromo")) {
					itemname = "images";
				} else {
					System.exit(0);
				}


				/*Resolve string node to actual node*/
				try {
					cqNode = CQDataConfig.getRootNode().getSession().getNode(cqNodeStr);
				} catch (Exception notanode) {
					System.out.println(cqNodeStr + " does not exist..skipping to next");
				}

				System.out.println("Node String getting processed is:" + cqNodeStr);

				/*Sorting the nodes, 
				 * example images-1,images-40,images-20
				 * becomes images-1,images-20,images-40
				 */
				mapnodes = sortnodes(cqNode, itemname); //Fix to sort


				/*Iterate through all sorted nodes*/
				for (Entry < Integer, Node > entry: mapnodes.entrySet()) {
					String child = entry.getValue().getPath().toString();

					/* Only process nodes like image-1,image-2...
                    	This is addressed in sortnodes,but still 
                        in place to double confirm we are not touching other nodes
                     */
					if (child.matches(".*\\/" + itemname + "-([0-9]+)?")) {
						keepflag = 1;
						PropertyIterator pi = entry.getValue().getProperties();

						/*Loop through image properties and capture image id, we will use this ids in dup check later*/
						while (pi.hasNext()) {
							Property p = pi.nextProperty();
							String prop = p.getName();
							if (prop.equalsIgnoreCase("sni:componentId")) {
								/* Duplicate image check */
								if (!items.add(gcqpv.getCQPropertyValue(p))) {
									System.out.println("Duplicate:" + entry.getValue().getPath() + " can be removed");
									dupnodes.add(entry.getValue());
									keepflag = 0;

								}
							}
						}
						/* Only capture unique nodes for renaming*/
						if (keepflag > 0) {
							fakemove(entry.getValue());
						}

					} //End child matches condition
				} // End of node iterator

				/*Deleting the duplicate nodes
				 * Note: Only the first node of duplicate wins
				 */
				for (Node ndel: dupnodes) {
					try {
						removeNode(ndel);
					} catch (Exception e) {
						System.out.println("Error when deleting node:" + e);
					}

				}

				/*Since node names are changed to some fake names
				 * lets recapture all changed nodes*/
				for (NodeIterator nia = cqNode.getNodes(); nia.hasNext();) {
					Node movedna = nia.nextNode();

					String movedchild = movedna.getPath().toString();

					System.out.println("node names " + movedchild);

					/* Only process nodes like image-xx*/
					if (movedchild.matches(".*\\/" + itemname + "-(xx(\\[[0-9]+\\])?)?")) {
						System.out.println("Moved node name:" + movedna.getPath());
						itemnodes.add(movedna);

					} //End movedchild matches condition
				} // End of moved node iterator

				System.out.println("Total unique nodes for renaming:" + itemnodes.size());
				/* Logic to rename all nodes */
				int i = 1;
				String start = null;
				for (Node n: itemnodes) {
					System.out.println("Before renaming the node:" + n.getPath());
					int lastdelimiter = n.getPath().lastIndexOf("-");
					if (lastdelimiter > 0) {
						start = n.getPath().substring(0, lastdelimiter);
					} else {
						start = n.getPath();
					}

					CQDataConfig.getRootNode().getSession().getWorkspace()
						.move(n.getPath(), start + "-" + i);
					start = null;
					i++;
				}
				items.clear(); // reset the items collection
				itemnodes.clear(); // reset the itemnodes collection
				dupnodes.clear(); //reset dupnodes arraylist
				mapnodes.clear();//reset mapnodes collection
			}
			CQDataConfig.getRootNode().getSession().save();
			bufRead.close();
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage(
				" Exception", e));
		}

	}



	public static void removeNode(Node Removenode)
	throws RepositoryException {
		try {
			Removenode.remove();
			Removenode.getSession().save();
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage(
				"Exception", e));
		}

	}

	public static void fakemove(Node nodestr) throws RepositoryException {

		System.out.println("fakemove:" + nodestr.getPath());
		String stringtoreplace = nodestr.getPath().substring(
		nodestr.getPath().lastIndexOf("/") + 1,
		nodestr.getPath().length());
		String replacewith = "images-xx";
		System.out.println(nodestr.getPath().replace(stringtoreplace, replacewith));
		String temppath = nodestr.getPath().replace(stringtoreplace, replacewith);

		CQDataConfig.getRootNode().getSession().getWorkspace()
			.move(nodestr.getPath(), temppath);
	}

	public static Map < Integer, Node > sortnodes(Node nodestr, String subtext) throws RepositoryException {
		Map < Integer, Node > orderedmap = new TreeMap < Integer, Node > (); //Using treemap to sort elements
		int delimiterposition = 0;
		for (NodeIterator nia = nodestr.getNodes(); nia.hasNext();) {
			Node na = nia.nextNode();
			String subnodestr = na.getPath().toString();

			/* Only process nodes like image-1,image-2...*/
			if (subnodestr.matches(".*\\/" + subtext + "-([0-9]+)?")) {
				delimiterposition = subnodestr.lastIndexOf("-");
				orderedmap.put(Integer.parseInt(subnodestr.substring(delimiterposition + 1, subnodestr.length())), na);
			}

		}

		return orderedmap;
	}


}