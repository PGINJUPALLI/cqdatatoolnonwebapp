package com.sni.home.CQDataTool.updates;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.GetCQPropertyValue;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/* An example input file content
 * /content/diy-com/en/blog-cabin/how-tos/pavan-test/jcr:content/tools
 * /content/diy-com/en/blog-cabin/how-tos/pavan-test/jcr:content/materials
 * 
 * Append following to CQDataUpdateFactory
 *     	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("renamenode")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   RenameNode.rename(cqdataconfobj);
    	   }
 * 
 * Add following to Beanfile
 *     <bean id="Update" class="com.sni.home.CQDataTool.config.CQDataConfig">
        <property name="infile" value="C:/files/outputfiles/input.txt" />
        <property name="updatetype" value="renamenode" />
        </property>
    </bean>
    
  *Program Argument: update
 */
public class RenameNode {

	private static final Logger log = LoggerFactory.getLogger(RenameNode.class);

	public static void rename(CQDataConfig cfg) throws Exception {

		try {
			System.out.println("The Input update file name is: " + cfg.getInfile());
			FileReader input = new FileReader(cfg.getInfile());
			BufferedReader bufRead = new BufferedReader(input);
			String myLine = null;
			Node cqNode = null;
			String itemname = null;
			Set < Node > itemnodes = new LinkedHashSet < Node > (); //Fix: converted to LinkedHashSet to preserve order
			Set < String > items = new LinkedHashSet < String > (); //Fix: converted to LinkedHashSet to preserve order
			GetCQPropertyValue gcqpv = new GetCQPropertyValue();

			while ((myLine = bufRead.readLine()) != null) {
				String[] param = myLine.split(cfg.getDelimit(), -1);
				for (int i = 0; i < param.length; i++) {
					// log.info("Arg "+i+" is :"+param[i].toString()+"\n");
				}


				String cqNodeStr = param[0].toString();
				String extension = cqNodeStr.substring(cqNodeStr.lastIndexOf("/") + 1);

				if (extension.equalsIgnoreCase("tools")) {
					itemname = "tool";
				} else if (extension.equalsIgnoreCase("materials")) {
					itemname = "material";
				} else {
					System.exit(0);
				}

				try {
					cqNode = CQDataConfig.getRootNode().getSession().getNode(cqNodeStr);
				} catch (Exception notanode) {
					System.out.println(cqNodeStr + " does not exist..skipping to next");
				}

				System.out.println("Node String getting processed is:" + cqNodeStr);

				for (NodeIterator nia = cqNode.getNodes(); nia.hasNext();) {
					Node na = nia.nextNode();

					String child = na.getPath().toString();

					if (child.matches(".*\\/"+itemname+"-([0-9]+)?"))
					{

						System.out.println(child.substring(
						child.lastIndexOf("/") + 1, child.length()));
						itemnodes.add(na);

						PropertyIterator pi = na.getProperties();
						while (pi.hasNext()) {
							Property p = pi.nextProperty();
							String prop = p.getName();
							if (prop.equalsIgnoreCase("name")) {
								items.add(gcqpv.getCQPropertyValue(p));
							}
						}

					}
				}

				removeNodes(itemnodes);
				itemnodes.clear(); //Fix:reset the itemnodes collection
				int i = 1;
				for (String itemvar: items) {
					System.out.println("Item:" + itemvar);
					System.out.println(cqNodeStr + "/" + itemname + "-" + i);
					addNode(cqNode, itemname + "-" + i, itemvar);
					i++;
				}
				items.clear();//Fix:reset the items collection

			}
			CQDataConfig.getRootNode().getSession().save();
			bufRead.close();
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage(
				"Run Update Exception", e));
		}

	}

	public static Node addNode(Node nodepath, String nodename, String propval)
	throws RepositoryException {

		Node resNode = nodepath.addNode(nodename, "nt:unstructured");
		resNode.addMixin("mix:referenceable"); //Fix: add mixins
		resNode.addMixin("crxdao:Tracked");//Fix: add mixins, note:this also will generate uuid
		resNode.setProperty("name", propval);
		resNode.getSession().save();
		Node daoNode = resNode.addNode("crxdao:meta", "nt:unstructured");
		daoNode.setProperty("crxdao:key", "supplyType");
		daoNode.setProperty("crxdao:stub", false);
		daoNode.getSession().save();
		return daoNode;
	}

	public static Node removeNodes(Set < Node > Removenodes)
	throws RepositoryException {
		for (Node nodeRemove: Removenodes) {
			nodeRemove.remove();
			nodeRemove.getSession().save();
		}
		return null;
	}

}