package com.sni.home.CQDataTool;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.interfaces.CQDataObject;
import com.sni.home.CQDataTool.interfaces.Connection;
import com.sni.home.CQDataTool.models.CQConnectionImpl;
import com.sni.home.CQDataTool.models.CQDataFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pavan Ginjupalli
 *         Date: 05/29/14
 *         Tripp Shewmake
 *         Date: 06/10/2014
 */
public class CQMain {

	static Logger log = LoggerFactory.getLogger(CQMain.class);

	/**
     * Main
     * Environment variable can be: DEV or LOCAL or PROD etc,
     * Program Arguments
     * Argument:1
     - for update argument is UPDATE
     - for report argument is REPORT
     - for delete argument is DELETE
     * Argument:2
     - beanname, example:genericreport or showreport or Update or Delete
     * this will load settings for that environment.
     * @param args Optional environment to run against LOCAL, DEV etc
     */

	public static void main(final String[] args) {

		log.info("starting application");
		Connection connection = null;
		String optype = null;
		String beanname = null;
		try {
			connection = new CQConnectionImpl();
			connection.login();
			CQDataConfig cqdoconf = new CQDataConfig();
			cqdoconf.setRootNode(connection.getNode());
			optype = args[0].toString().toLowerCase();
			beanname = args[1].toString().toLowerCase();
			log.info("operation type:"+optype);
			log.info("bean name:"+beanname);
			CQDataObject cqdo = CQDataFactory.getCQObject(optype);
			cqdo.processCQObject(beanname);
			connection.logout();
			log.info("Application exiting under normal conditions");
			System.exit(0);
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage("Exception in Main", e));
			System.exit(1);
		}
	} //End of Main
} //End of class CQMain