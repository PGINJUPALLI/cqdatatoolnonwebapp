package com.sni.home.CQDataTool.models;

import com.sni.home.CQDataTool.interfaces.CQDataObject;

public class CQDataFactory {
	
	public static CQDataObject getCQObject(String type){
		
		if(type.equalsIgnoreCase("report")) {
			System.out.println("Calling CQDataReportFactory");
			return new CQDataReportFactory();
		}
		else if (type.equalsIgnoreCase("update")){
			System.out.println("Calling CQDataUpdateFactory");
			return new CQDataUpdateFactory();
		}

		else if (type.equalsIgnoreCase("delete")){
			System.out.println("Calling CQNodeMangerFactory");
			return new CQNodeManagerFactory();
		}
		
		
		return null;
		
	}

}
