package com.sni.home.CQDataTool.models;


import com.sni.home.CQDataTool.config.CQConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.interfaces.Connection;

import org.apache.jackrabbit.commons.JcrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

/**
 * Created with IntelliJ IDEA.
 * User: Tripp Shewmake
 * Date: 6/10/14
 * Time: 9:13 PM
 * Returns a recipe template based on a type requested
 */
public class CQConnectionImpl implements Connection{

    static Logger log = LoggerFactory.getLogger(CQConnectionImpl.class);
    private CQConfig cqConfig;
    private Repository repository;
    private Session session;
    public Node rootnode;
    
    public Node getNode()
    {
    	log.info ("The root node is "+rootnode); 
    	return rootnode;
    }
    

    public void login() throws Exception{
        log.info("Logging in to instance");
        try {
            session = repository.login(new SimpleCredentials(cqConfig.getUser(), cqConfig.getPassword().toCharArray()));
            rootnode = session.getRootNode();
            log.info ("The root node is " + rootnode); 
        } catch (Exception e) {
            log.error(ExceptionMsg.formatExceptionMessage("Error logging into instance", e));
            throw e;
        }
        log.info ("Login successful on " + cqConfig.getHost());
    }

    public void logout() {
        try {
        	session.save();
        	log.info("Session has been saved");
            } catch (Exception e)
          {
            log.info("Logging out of " + cqConfig.getHost());
            session.logout();
          }
        session.logout();
    }
    
    public CQConnectionImpl() throws Exception {
        log.info("Get Spring application context");
        System.out.println("Before the call for Application Context");
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        System.out.println("After the call for Application Context");
        log.info("Load CQ configuration values");
        this.cqConfig = (CQConfig) context.getBean("CQConfig");
        try {
            log.info("CQ Host: " + cqConfig.getHost());
            log.info("CQ User: " + cqConfig.getUser());
            //log.info("CQ Password: " + cqConfig.getPassword());
        } catch (Exception e) {
            log.error("CQ configuration failed to load");
            context.close();
            throw e;
            
        }
        context.close();

        String repoUrl = cqConfig.getHost() + cqConfig.getPath();
        log.info("Configuring repository " + repoUrl);

        try {
            this.repository = JcrUtils.getRepository(repoUrl);
        } catch (RepositoryException re) {
            log.error(ExceptionMsg.formatExceptionMessage("Repository Exception", re));
            throw re;
        } catch (Exception e) {
            log.error(ExceptionMsg.formatExceptionMessage("Unknown Exception",e));
            throw e;
        }
    }
}
