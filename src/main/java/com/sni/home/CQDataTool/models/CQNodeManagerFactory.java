package com.sni.home.CQDataTool.models;


import com.sni.home.CQDataTool.utils.packageOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.deletes.Cleanupuserdefinednodes;
//import com.sni.home.CQDataTool.deletes.ManageNodes;
import com.sni.home.CQDataTool.interfaces.CQDataObject;

	public class CQNodeManagerFactory   implements CQDataObject{
		
		private static final Logger log =  LoggerFactory.getLogger(CQNodeManagerFactory.class.getSimpleName());


	    public void processCQObject(String beanname) {    	
	    	    	
	    	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("cqdoconfig.xml");
	    	CQDataConfig cqdataconfobj = (CQDataConfig) context.getBean(beanname);
	    	  	

	       try{
	    	   
	    	//ManageNodes.deletenode(cqdataconfobj);
	    	//Cleanupuserdefinednodes.deletenode(cqdataconfobj);
               packageOperations.addFilters(cqdataconfobj);
	       }
	        catch (Exception e)
	        {
	            log.error(ExceptionMsg.formatExceptionMessage("CQNodeManagerFactory Exception", e));
	            context.close();
	        }
	        context.close();

	    }



}
