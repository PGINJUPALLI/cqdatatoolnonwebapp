package com.sni.home.CQDataTool.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.interfaces.CQDataObject;
import com.sni.home.CQDataTool.move.MoveNode;
import com.sni.home.CQDataTool.updates.AddMissingProperties;
import com.sni.home.CQDataTool.updates.CorrectOrdering;
import com.sni.home.CQDataTool.updates.RenameNode;
import com.sni.home.CQDataTool.updates.UpdateAdKey;
import com.sni.home.CQDataTool.updates.UpdateImageMeta;
import com.sni.home.CQDataTool.updates.UpdateMultiValNode;
import com.sni.home.CQDataTool.updates.UpdatePropertyForNode;

public class CQDataUpdateFactory  implements CQDataObject{
	
	private static final Logger log =  LoggerFactory.getLogger(CQDataUpdateFactory.class.getSimpleName());


    public void processCQObject(String beanname) {    	
    	    	
    	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("cqdoconfig.xml");
    	CQDataConfig cqdataconfobj = (CQDataConfig) context.getBean(beanname);
    	  	

       try{
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("image")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   UpdateImageMeta.UpdateImageProperty(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("imageprop")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   AddMissingProperties.addProperty(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("adkey")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   UpdateAdKey.updateProperty(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("property")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   UpdatePropertyForNode.updateProperty(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("multproperty")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   UpdateMultiValNode.updateProperty(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("renamenode")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   RenameNode.rename(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("movenode")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   MoveNode.moveNode(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("ordernode")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   CorrectOrdering.rearrange(cqdataconfobj);
    	   }
       }
        catch (Exception e)
        {
            log.error(ExceptionMsg.formatExceptionMessage("Run CQ Update Exception", e));
            context.close();
        }
        context.close();

    }


}
