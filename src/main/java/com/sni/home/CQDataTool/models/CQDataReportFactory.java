package com.sni.home.CQDataTool.models;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.interfaces.CQDataObject;
import com.sni.home.CQDataTool.reports.*;
//import com.sni.home.CQDataTool.reports.CQReportNodeStr;
//import com.sni.home.CQDataTool.reports.AdhocQueryReport;
//import com.sni.home.CQDataTool.reports.ImageReport;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CQDataReportFactory implements CQDataObject {

	
	private static final Logger log =  LoggerFactory.getLogger(CQDataReportFactory.class);


    public void processCQObject(String beanname) {   
    	
    	    	
    	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("cqdoconfig.xml");
    	CQDataConfig cqdataconfobj = (CQDataConfig) context.getBean(beanname);
    	  	

       try{
    	   //CQGenericReport.cqgenericReport(cqdataconfobj);
    	  // CQReport.cqReport(cqdataconfobj);

    	  // GenericReport.cqReport(cqdataconfobj);
           AllAssetsReport.allassetsReport(cqdataconfobj);


    	  // PhotoGalleryReport.cqReport(cqdataconfobj);
    	  // ImageReport.imageReport(cqdataconfobj);
    	  // CQReportNodeStr.getReport(cqdataconfobj);
    	  // AdhocQueryReport.adhocReport(cqdataconfobj);
    	  // Checkforsubnodesbyname.check(cqdataconfobj);
       }
        catch (Exception e)
        {
            log.error(ExceptionMsg.formatExceptionMessage("Run Report Exception", e));
            context.close();
        }
        context.close();

    }

}
