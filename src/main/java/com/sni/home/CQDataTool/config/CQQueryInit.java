package com.sni.home.CQDataTool.config;

import javax.jcr.Node;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CQQueryInit {
	
	static  Logger log = LoggerFactory.getLogger(CQQueryInit.class);

	public static Query initQuery(CQDataConfig cfg) 
	 {
		Node n = CQDataConfig.getRootNode();
		
		 try {		
			 
				 QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
	             String queryString = cfg.getQuery();
	             Query query = queryManager.createQuery(queryString,Query.JCR_SQL2); 
	             log.info("Your query is:"+queryString.toString());
	             return query;
            
		 	}
		 catch (Exception e) {
	            log.error(ExceptionMsg.formatExceptionMessage("Run CQQueryInit Exception", e));
	            return null;
	        }
	 }
	

}
