package com.sni.home.CQDataTool.config;

import javax.jcr.Node;

public class CQDataConfig {
	
	private static Node rootNode;	
	private String updatetype;	
	private String outfile;
	private String infile;
	private String[] bucket;
	private String[] years;
	private String columns;
	private String delimit;
	private String query;
	private String[] fields;
    private String pkgpath;



	public static  Node getRootNode() {
		return rootNode;
	}

	public void setRootNode(Node rootNode) {
		CQDataConfig.rootNode = rootNode;
	}
	
	public String[] getBucket() {
		return bucket;
	}

	public void setBucket(String[] bucket) {
		this.bucket = bucket;
	}

	public String getOutfile() {
		return outfile;
	}

	public void setOutfile(String outfile) {
		this.outfile = outfile;
	}

	public String[] getYears() {
		return years;
	}

	public void setYears(String[] years) {
		this.years = years;
	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public String getDelimit() {
		return delimit;
	}

	public void setDelimit(String delimit) {
		this.delimit = delimit;
	}

	public String getUpdatetype() {
		return updatetype;
	}

	public void setUpdatetype(String updatetype) {
		this.updatetype = updatetype;
	}

	public String getInfile() {
		return infile;
	}

	public void setInfile(String infile) {
		this.infile = infile;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

    public String getPkgpath() {
        return pkgpath;
    }

    public void setPkgpath(String pkgpath) {
        this.pkgpath = pkgpath;
    }

}
