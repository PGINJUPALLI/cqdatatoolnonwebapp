package com.sni.home.CQDataTool.reports;

import java.io.FileWriter;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.GetCQPropertyValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TagReport {
    
    private static final Logger log = LoggerFactory.getLogger(TagReport.class.getSimpleName());
    
    public static void tagReport(CQDataConfig cfg) {
        String propvalue = "";
        String primarytag = "";
        
        try {
            Node n = CQDataConfig.getRootNode();
            FileWriter fw = new FileWriter(cfg.getOutfile());
            // Obtain the query manager for the session via the workspace ...
            QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
            fw.append(cfg.getColumns()+"\n");
			
                String queryString = cfg.getQuery();
                Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);                
                																			System.out.println("sql statement is:" + query.getStatement());
                // Execute the query and get the results ...
                javax.jcr.query.QueryResult result = query.execute();
                RowIterator rows = result.getRows();
                																			System.out.println("Query found "+result.getRows().getSize()+" rows");
                GetCQPropertyValue cqp = new GetCQPropertyValue();
                while (rows.hasNext()) {
                    
                    Row row = rows.nextRow();
                    Node na = row.getNode();
                    na.getPath();
                    
                    
                    for(NodeIterator na1 = na.getNodes();na1.hasNext();){
                    	Node n1=na1.nextNode();
                    	//System.out.println("Node:"+n1.getName());
                    	
                    	if(n1.getName().equals("tags")){
                    		
                    		PropertyIterator pi = n1.getProperties();
                    		//System.out.println("Node1:"+n1.getPath()+" contains "+n1.getProperties().getSize()+" properities");
                    		 
                    		  while (pi.hasNext()) {
                                  Property p = pi.nextProperty();
                                  if(p.getName().toLowerCase().equals("primarytaggroup")){ 
                                  primarytag=cqp.getCQPropertyValue(p);
                                  }
                               }
                    		  
                    		  for(NodeIterator na2=n1.getNodes();na2.hasNext();)
                    		  {
                    			  Node n2 = na2.nextNode();
                    			  //System.out.println("Node2 is:"+n2.getName());
                    			  PropertyIterator pi2 = n2.getProperties();
                    			  while (pi2.hasNext()) {
                                      Property px = pi2.nextProperty();
                                      if(px.getName().toLowerCase().equals("additionaltaggroup")){
                                    	  propvalue=cqp.getCQPropertyValue(px);
                                    	  if(primarytag.equals(propvalue)){
                                    		 System.out.println("There is a tag match:"+n1.getPath()+" ------> "+propvalue) ;
                                    		 fw.append(n1.getPath());
                                    		 fw.append(cfg.getDelimit()+propvalue);
                                    		 fw.append("\n");
                                    		 propvalue="";
                                    	  }
                                      }
                                  }
                    		  }
                    		                      		
                  }//End of tags
                    		  
                    	}//End of na1 
                    fw.append("\n");

                }// End While loop for rows.hasnext()

            fw.flush();
            fw.close();
            } catch (Exception e) {
            log.error(ExceptionMsg.formatExceptionMessage("Run CQReport Exception", e));
        }
        
    }
    
}// End CQReport