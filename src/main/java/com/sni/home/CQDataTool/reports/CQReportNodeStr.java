package com.sni.home.CQDataTool.reports;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.jcr.Node;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.RecurseCQ;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

		/**
		 * @author Pavan Ginjupalli Date: 10/17/14
		 */
	public class CQReportNodeStr {

		    private static final Logger log = LoggerFactory.getLogger(CQReportNodeStr.class);

		    public static void getReport(CQDataConfig cfg) throws Exception {   
		    	
				Set<Node> childnodes= new HashSet<Node>();
				
				ArrayList<String> aggrstr = new ArrayList<String>();
				
				int i=0;

		        try {
		        	
		        	log.info("The file name is: "+cfg.getInfile());
		        	
		            FileReader input = new FileReader(cfg.getInfile());
		            
		            BufferedReader bufRead = new BufferedReader(input);
		            
		            FileWriter fw = new FileWriter(cfg.getOutfile(),true);
		            
		            String myLine = null;
		            
		            String parentnode = null;
		            
		            while ((myLine = bufRead.readLine()) != null)
		            {
		            //System.out.println("Start while loop");
		               	String NodeStr 	= myLine.toString();
		                
		                log.info(i+":Processing the node: "+NodeStr);
		                
		                try {
		                Node n = CQDataConfig.getRootNode().getSession().getNode(NodeStr);
		                
		                parentnode = n.getPath().toString();
		                
						childnodes=RecurseCQ.recursechildren(n, cfg);
						
						aggrstr = CQReport.processchildnodes(childnodes,cfg);
			
						  fw.append(parentnode);
						
						  for (String outval:aggrstr)
						  {
							  fw.append(outval);
							  System.out.println(outval);
						  }
						  fw.append("\n");
		                }
		                catch(Exception PathNotFoundException){
		                	fw.append("\n");
		                	fw.append(NodeStr+"$Error$"+"\n");
		                	
		                }
						  
						  i=i+1;
						  
						  
		            }	
	  
		            bufRead.close();
		            fw.close();
		        }
		        catch (Exception e)
		        {
		        	System.out.println(e);
		            log.error(ExceptionMsg.formatExceptionMessage("Run node based report Exception", e));
		        }

		    }

}
