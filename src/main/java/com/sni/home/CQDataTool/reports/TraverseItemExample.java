package com.sni.home.CQDataTool.reports;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import javax.jcr.util.TraversingItemVisitor;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.RecurseCQ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TraverseItemExample extends TraversingItemVisitor{
		
	private static final Logger log =  LoggerFactory.getLogger(AdhocQueryReport.class);
	

	@Override
	protected void entering(Property property, int level)
			throws RepositoryException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void entering(Node node, int level)
			throws RepositoryException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void leaving(Property property, int level)
			throws RepositoryException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void leaving(Node node, int level) throws RepositoryException {
		// TODO Auto-generated method stub
		
	}

		
		public static void adhocReport(CQDataConfig cfg) 

			{
			
			
				try 
				{
	    	
				Node n = CQDataConfig.getRootNode();
	    	
				QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
				
				ArrayList<String> assetid_array = new ArrayList<String>();
				
				FileWriter fw = new FileWriter(cfg.getOutfile());
				
	            FileReader input = new FileReader(cfg.getInfile());
	            
	            BufferedReader bufRead = new BufferedReader(input);
	            
	            String myLine = null;
	            
	            QueryResult result;
	            
	            

	            while ((myLine = bufRead.readLine()) != null)
	            {
	               
	               	String QryStr 	= myLine.toString();
	                assetid_array.add(QryStr);
	               
	            }
	            
	            log.info("No of lines in the buffer:"+assetid_array.size());
				fw.append(cfg.getColumns()+"\n");
	    			
				Set<Node> childnodes= new HashSet<Node>();
				
				ArrayList<String> aggrstr = new ArrayList<String>();
				
				String parentnode=null;
				
				Row row;
	            
	    			for (String assetid : assetid_array)
	    			{
	    								String queryString = cfg.getQuery().replaceAll("ASSETID", assetid+".0");
	    								Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);
	    								result = query.execute();
	    								log.info("Query is:"+queryString.toString());
	    								RowIterator rows = result.getRows();
	    								log.info("Query found :"+rows.getSize()+" rows");
	    								
	    							
	    								while(rows.hasNext())
	    								{
	    									
	    									row = rows.nextRow();
	    									
	    									Node pnode = row.getNode();
	    									
	    									log.info(pnode.getPath().toString());
	    									
	    									parentnode= pnode.getPath().toString();
	    									
	    									childnodes=RecurseCQ.recursechildren(pnode, cfg);
	    								
	    									aggrstr = CQReport.processchildnodes(childnodes,cfg); 
	    									
	    									

		    								  for (String outval:aggrstr)
		    								  {
		    									  fw.append(parentnode+outval.toString()+"\n");
		    								  }
		    								  
	    									
	    								}
	    								
	    								
	    								  
	    								}// End of for loop assetid_array
	    								
	                                    
	    	
	    		
				bufRead.close();	 
				fw.close();	 
					 
				}
				catch (Exception e)
				{
		        log.error(ExceptionMsg.formatExceptionMessage("Run Report Exception", e));
				}
	       
	}

}
