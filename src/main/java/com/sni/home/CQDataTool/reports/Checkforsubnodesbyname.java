package com.sni.home.CQDataTool.reports;

import java.io.FileWriter;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Checkforsubnodesbyname {


	private static Logger log = LoggerFactory.getLogger(Checkforsubnodesbyname.class);

	public static void check(CQDataConfig cfg) throws Exception {

		int rowssize = 0;
		int counter = 0;


		try {

			Node n = CQDataConfig.getRootNode();

			FileWriter fw = new FileWriter(cfg.getOutfile());

			// Obtain the query manager for the session via the workspace ...
			QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();

			String queryString = cfg.getQuery();

			Query query = queryManager.createQuery(queryString, Query.JCR_SQL2);

			log.info("sql statement is:" + query.getStatement());

			// Execute the query and get the results ...
			javax.jcr.query.QueryResult result = query.execute();

			RowIterator rows = result.getRows();

			rowssize = (int) result.getRows().getSize();

			log.info("Query found " + result.getRows().getSize() + " rows");

			while (rows.hasNext())

			{
				Row row = rows.nextRow();

				try {
					Node na = row.getNode();
					System.out.println("Checking Node [" + counter + " of " + rowssize + "] :" + na.getPath());

					if (!contain(na, "cq5dam.thumbnail")) {
						System.out.println("no thumbnail rendition for:" + na.getPath());
						fw.append(na.getPath().toString());
						fw.append("\n");
					}

				} catch (Exception e) {
					log.error(ExceptionMsg.formatExceptionMessage("Run CQReport Exception", e));
				}
				counter++;
			}
			fw.flush();
			fw.close();
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage("Run CQReport Exception", e));
		}
	}


	public static boolean contain(Node nc, String subnodename) throws Exception {
		int i = 0;
		int renditionsfound = 0;
		for (NodeIterator nia = nc.getNodes(); nia.hasNext();) {

			Node renditionnode = nia.nextNode();

			if (renditionnode.getName().toString().matches("renditions")) {
				renditionsfound = 1;
				for (NodeIterator niaren = renditionnode.getNodes(); niaren.hasNext();) {
					Node thubnailnode = niaren.nextNode();

					if (thubnailnode.getName().toString().matches("cq5dam.thumbnail.*")) {
						i = i + 1;
					}
				}


			}
		}
		if ((i == 0) && (renditionsfound > 0)) {
			return false;
		} else {
			return true;

		}

	}
}