package com.sni.home.CQDataTool.reports;

import java.io.FileWriter;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.CleanImageMeta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageReport_Old {
	
	private static final Logger log =  LoggerFactory.getLogger(ImageReport_Old.class);
	
public static void imageReport(CQDataConfig cfg)
{
    String propvalue = "";
    String l_Field = "";
    String outPath = "";
    String outDCCopyright = "";
    String outSNICopyright = "";
    String outSNIDCCopyright = "";
	
   
    try {
    			Node n = CQDataConfig.getRootNode();
    			FileWriter fw = new FileWriter(cfg.getOutfile());
    			// Obtain the query manager for the session via the workspace ...
    			QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();

    			String delimiter=cfg.getDelimit();
    			fw.append(cfg.getColumns()+ "\n");
    			CleanImageMeta cleanIMeta = new CleanImageMeta();
        
    			for (String year : cfg.getYears())
    				{
    					System.out.printf("%s:\n", year);
            
    						for (String bucket : cfg.getBucket())
    							{
    								String queryString = cfg.getQuery().replaceAll("BUCKET", bucket);
    								queryString = queryString.replaceAll("YEAR", year);
    								Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);

    								// Execute the query and get the results ...
    								javax.jcr.query.QueryResult result = query.execute();
    								RowIterator rows = result.getRows();
                
    								while (rows.hasNext())
    									{
    										outPath = "";
    										outDCCopyright = "";
    										outSNICopyright = "";
    										outSNIDCCopyright = "";
    										Row row = rows.nextRow();
    										Node na = row.getNode();
    										outPath = na.getPath();
    										PropertyIterator pi = na.getProperties();
    										System.out.println("Node:"+outPath);
    										
    									                    
    											while (pi.hasNext())
    												{
    												Property p = pi.nextProperty();
    												l_Field = p.getName();
    												
    												
    												switch (l_Field) {
				
												                            case "dc:copyright":
												                                if (p.getDefinition().isMultiple()) {
												                                    Value[] values = p.getValues();
												                                    for (int i = 0; i < values.length; i++) {
												                                        propvalue = values[i].getString();
												                                        outDCCopyright = cleanIMeta.cleanImageMeta(propvalue);
												                                    }// End of values for loop
												                                } else {
												                                    propvalue = p.getValue().getString();
												                                    outDCCopyright = cleanIMeta.cleanImageMeta(propvalue);
												                                }// End if isMultiple
												                                break;
												
												                            case "sni:copyright":
												                                if (p.getDefinition().isMultiple()) {
												                                    Value[] values = p.getValues();
												                                    for (int i = 0; i < values.length; i++) {
												                                        propvalue = values[i].getString();
												                                        outSNICopyright = cleanIMeta.cleanImageMeta(propvalue);
												                                    }// End of values for loop
												                                } else {
												                                    propvalue = p.getValue().getString();
												                                    outSNICopyright = cleanIMeta.cleanImageMeta(propvalue);
												                                }// End if isMultiple
												                                break;
												                            case "sni:dc-copyright":
												                                if (p.getDefinition().isMultiple()) {
												                                    Value[] values = p.getValues();
												                                    for (int i = 0; i < values.length; i++) {
												                                        propvalue = values[i].getString();
												                                        outSNIDCCopyright = cleanIMeta.cleanImageMeta(propvalue);
												                                    }// End of values for loop
												                                } else {
												                                    propvalue = p.getValue().getString();
												                                    outSNIDCCopyright =cleanIMeta.cleanImageMeta(propvalue);
												                                }// End if isMultiple
												                                break;										                                
												                           
												
												                            default:
												                                break;
								                        				}// End switch statement

    												}// End While statement
						                    log.info(outPath + delimiter + outDCCopyright + delimiter+ outSNICopyright + delimiter + outSNIDCCopyright+ "\n");
						                    fw.append(outPath + delimiter + outDCCopyright + delimiter+ outSNICopyright + delimiter + outSNIDCCopyright + "\n");
    									}//End While Loop

    							}//End for loop buckets
    				}//End for loop years

        fw.close();
    	}
	    catch (Exception e)
	    {
	        log.error(ExceptionMsg.formatExceptionMessage("Run Report Exception", e));
	    }
       
}

}//End imageReport

