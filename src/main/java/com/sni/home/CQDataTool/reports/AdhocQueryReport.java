package com.sni.home.CQDataTool.reports;



	import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.jcr.Node;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.RecurseCQ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

	public class AdhocQueryReport {
		
	private static final Logger log =  LoggerFactory.getLogger(AdhocQueryReport.class);
		
		public static void adhocReport(CQDataConfig cfg) 

			{
				try 
				{
	    	
				Node n = CQDataConfig.getRootNode();
	    	
				QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
				
				ArrayList<String> assetid_array = new ArrayList<String>();
				
				FileWriter fw = new FileWriter(cfg.getOutfile());
				
	            FileReader input = new FileReader(cfg.getInfile());
	            
	            BufferedReader bufRead = new BufferedReader(input);
	            
	            String myLine = null;
	            
	            int recordno=0;
	            
	            QueryResult result;

	            while ((myLine = bufRead.readLine()) != null)
	            {
	               
	               	String QryStr 	= myLine.toString();
	                assetid_array.add(QryStr);
	               
	            }
	            int totalrecords=assetid_array.size();
	            log.info("No of lines in the buffer:"+totalrecords);
				fw.append(cfg.getColumns()+"\n");
	    			
				ArrayList<Node> childnodes= new ArrayList<Node>();
				
				ArrayList<String> aggrstr = new ArrayList<String>();
				
				String parentnode=null;
				
				
				
				Row row;
	            
	    			for (String assetid : assetid_array)
	    			{
	    								String queryString = cfg.getQuery().replaceAll("ASSETID", assetid);
	    								Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);
	    								result = query.execute();
	    								//log.info("Query is:"+queryString.toString());
	    								RowIterator rows = result.getRows();
	    								//log.info("Query found :"+rows.getSize()+" rows");
	    								
	    							
	    								while(rows.hasNext())
	    								{
	    									
	    									row = rows.nextRow();
	    									
	    									Node pnode = row.getNode();
	    									
	    									log.info(pnode.getPath().toString());
	    									
	    									parentnode= pnode.getPath().toString();
	    									
	    									/*childnodes=RecurseCQ.recursechildren(pnode, cfg);
	    								
	    									aggrstr = CQReport.processchildnodes(childnodes,cfg); 
	    									
	    									

		    								  for (String outval:aggrstr)
		    								  {
		    									  fw.append(parentnode+outval.toString()+"\n");
		    								  }
		    								  */
	    									
	    									 System.out.println("Processing "+recordno+ " of "+totalrecords);
	    									 fw.append(assetid.toString()+"$"+parentnode+"\n");
	    								}
	    								
	    								recordno=recordno+1;
	    								  
	    								}// End of for loop assetid_array
	    								
	                                    
	    	
	    		
				bufRead.close();	 
				fw.close();	 
					 
				}
				catch (Exception e)
				{
		        log.error(ExceptionMsg.formatExceptionMessage("Run Report Exception", e));
				}
	       
	}


}
