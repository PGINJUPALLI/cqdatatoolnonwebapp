
	package com.sni.home.CQDataTool.reports;

	import java.io.FileWriter;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

	public class PhotoGalleryReport {

		private static final Logger	log	= LoggerFactory.getLogger(CQReport.class.getSimpleName());

		public static void cqReport(CQDataConfig cfg) {
			int countimages=0;
						
			try {
				Node n = CQDataConfig.getRootNode();
				FileWriter fw = new FileWriter(cfg.getOutfile());
				// Obtain the query manager for the session via the workspace ...
				QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
				fw.append(cfg.getColumns() + "\n");

				String queryString = cfg.getQuery();
				Query query = queryManager.createQuery(queryString, Query.JCR_SQL2);
				System.out.println("sql statement is:" + query.getStatement());
				// Execute the query and get the results ...
				javax.jcr.query.QueryResult result = query.execute();
				RowIterator rows = result.getRows();
				System.out.println("Query found " + result.getRows().getSize()+ " rows");
				while (rows.hasNext()) {
					Row row = rows.nextRow();
					Node na = row.getNode();
					na.getPath();
					String npath = na.getPath().toString();
					
					NodeIterator ni = na.getNodes();
					System.out.println("Node:" + na.getPath() + " contains "+ na.getProperties().getSize() + " properities");


					while (ni.hasNext()) {
						Node p = ni.nextNode();
						System.out.println("Found property:"+p.getName().toString());
						String sn = p.getPath().toString();
						sn=sn.replace(npath, "");
						if(sn.matches("(?i)/image.*")){
							countimages=countimages+1;
							//fw.append(npath+cfg.getDelimit()+sn+"\n");
						}
						
					}// End While statement
					fw.append(npath+cfg.getDelimit()+countimages+"\n");
					countimages=0;
				}// End While loop for rows.hasnext()

				fw.flush();
				fw.close();
			} catch (Exception e) {
				log.error(ExceptionMsg.formatExceptionMessage("Run CQReport Exception", e));
			}

		}

	}// End PhotoGalleryReport

