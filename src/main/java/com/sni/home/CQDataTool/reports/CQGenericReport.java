package com.sni.home.CQDataTool.reports;

import java.io.FileWriter;

import javax.jcr.Node;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CQGenericReport {
	
private static final Logger log =  LoggerFactory.getLogger(CQGenericReport.class);
	
	public static void cqgenericReport(CQDataConfig cfg)

		{
			try 
			{
    	
			Node n = CQDataConfig.getRootNode();
    	
			QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
			
			FileWriter fw = new FileWriter(cfg.getOutfile());
			
			fw.append(cfg.getColumns()+"\n");
    			
			
    								String queryString = cfg.getQuery();
    								Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);
    								log.info("Your query is:"+queryString.toString());
    								CQReport.cqReport(cfg,query,"generic",fw);
    								//Test.cqReport(cfg,query,"generic",fw);
    								fw.close();	 
				 
			}
			catch (Exception e)
			{
	        log.error(ExceptionMsg.formatExceptionMessage("Run Report Exception", e));
			}
       
}

}


