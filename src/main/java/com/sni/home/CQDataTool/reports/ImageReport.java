package com.sni.home.CQDataTool.reports;

import java.io.FileWriter;

import javax.jcr.Node;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageReport {
	
private static final Logger log =  LoggerFactory.getLogger(ImageReport.class);
	
	public static void imageReport(CQDataConfig cfg)

		{
			try 
			{
    	
			Node n = CQDataConfig.getRootNode();
    	
			QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
			
			FileWriter fw = new FileWriter(cfg.getOutfile());
			
			fw.append(cfg.getColumns()+"\n");
    			
			for (String year : cfg.getYears())
    		{
				System.out.printf("%s:\n", year);
            
    						for (String bucket : cfg.getBucket())
    							{
    								String queryString = cfg.getQuery().replaceAll("BUCKET", bucket);
    								queryString = queryString.replaceAll("YEAR", year);
    								Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);
    								log.info("Your query is:"+queryString.toString());
    								CQReport.cqReport(cfg,query,"image",fw);
                                    
    							}
    				}
				 
			fw.close();	 
				 
			}
			catch (Exception e)
			{
	        log.error(ExceptionMsg.formatExceptionMessage("Run Report Exception", e));
			}
       
}

}


