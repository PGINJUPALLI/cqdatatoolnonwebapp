package com.sni.home.CQDataTool.reports;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.db.sendtodb;
import com.sni.home.CQDataTool.utils.CleanString;
import com.sni.home.CQDataTool.utils.GetCQPropertyValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenericReport {

	private static final Logger	log	= LoggerFactory.getLogger(GenericReport.class);

	public static void cqReport(CQDataConfig cfg) {
		
		String propvalue = "";
		String insertstmt="";
		String columnsString="CQPath";
		
		try {
			
			Node n = CQDataConfig.getRootNode();
			
			FileWriter fw = new FileWriter(cfg.getOutfile());
			
			// Obtain the query manager for the session via the workspace ...
			QueryManager queryManager = n.getSession().getWorkspace().getQueryManager();
			
			
			for (String lfield: cfg.getFields()) {
				if(cfg.getFields().length!=0)
				{
					columnsString=columnsString+cfg.getDelimit()+lfield;			
				}
				else
				{
					columnsString=columnsString+lfield;
				}
				}
			fw.append(columnsString+"\n");

			String queryString = cfg.getQuery();
			
			Query query = queryManager.createQuery(queryString, Query.JCR_SQL2);
			
			log.info("sql statement is:" + query.getStatement());
			
			// Execute the query and get the results ...
			javax.jcr.query.QueryResult result = query.execute();
			
			RowIterator rows = result.getRows();
			
			log.info("Query found " + result.getRows().getSize()+ " rows");
			
			while (rows.hasNext()) 
			
			{
				Row row = rows.nextRow();
					
				try 
					{
						Node na = row.getNode();
						
					    na.getPath();
					    
					    fw.append(na.getPath());
					
					    PropertyIterator pi = na.getProperties();
					    //System.out.println("Node:" + na.getPath() + " contains "+ na.getProperties().getSize() + " properities");
	
					    GetCQPropertyValue cqp = new GetCQPropertyValue();
	
					    HashMap<String, Property> map = new HashMap<String, Property>();
	
						while (pi.hasNext()) 
						{
							Property p = pi.nextProperty();
							
							map.put(p.getName().toString(), p);
						}// End While statement
	
						for (String lfield : cfg.getFields()) 
						{
							for (String key : map.keySet()) {
								if (lfield.equalsIgnoreCase(key)) {
									propvalue = cqp.getCQPropertyValue(map.get(key));
								}
		
							}
							
						
					/*		
						insertstmt = "INSERT INTO CQ_REPORT_CONTENT (" +
																		"CQPATH,SITE,ASSET_TYPE,FIELD1,FIELD2,FIELD3,FIELD4,FIELD5,FIELD6,FIELD7,FIELD8,FIELD9,FIELD10," +
																		"FIELD11,FIELD12,FIELD13,FIELD14,FIELD15,FIELD16,FIELD17,FIELD18,FIELD19,FIELD20," +
																		"FIELD21,FIELD22"+
																		") VALUES (" +	
																		propvalue +																		
																		");"
																		;
						sendtodb.insert(insertstmt);
						
						insertstmt = "";
						
					*/	
						CleanString cleanpropval = new CleanString();
						
						propvalue=cleanpropval.clean(propvalue);
							
						fw.append(cfg.getDelimit() + propvalue);
						
						propvalue = "";
						
						
						}
						
						
																		
	
						fw.append("\n");
					}
					catch(PathNotFoundException p){
	                	log.info(row.getNode()+" not found,skipping");
	                }
				
				}// End While loop for rows.hasnext()

			fw.flush();
			fw.close();
			
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage("Run CQReport Exception", e));
		}

	}

}// End Playlists
