package com.sni.home.CQDataTool.deletes;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

	/**
	 * @author Pavan Ginjupalli Date: 10/17/14
	 */
	public class Cleanupuserdefinednodes {

	    private static final Logger log = LoggerFactory.getLogger(Cleanupuserdefinednodes.class);

	    public static void deletenode(CQDataConfig cfg) throws Exception {   	
	    	

	        try {
	        	
	        	log.info("The file name is: "+cfg.getInfile());
	        	
	            FileReader input = new FileReader(cfg.getInfile());
	            
	            BufferedReader bufRead = new BufferedReader(input);
	            
	            String myLine = null;

	            while ((myLine = bufRead.readLine()) != null)
	            {
	               
	               	String NodeStr 	= myLine.toString();
	                
	                log.info("Deleting the node: "+NodeStr);
	                
	                try{
	                
	                Node n = CQDataConfig.getRootNode().getSession().getNode(NodeStr);
	                
	                if (!n.equals(null))
	                {
	                
	                	n.remove();
	                	
	                	CQDataConfig.getRootNode().getSession().save();
	                
	                }
	                }
	                catch(PathNotFoundException p){
	                	log.info("Not found, Skipping =====>Node "+NodeStr);
	                }
	                
	              }
	          
	            bufRead.close();
	        }
	        catch (Exception e)
	        {
	            log.error(ExceptionMsg.formatExceptionMessage("Run userdefined node delete Exception", e));
	        }

	    }
	
}
