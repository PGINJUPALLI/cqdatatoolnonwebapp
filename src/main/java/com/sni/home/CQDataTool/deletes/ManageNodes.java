package com.sni.home.CQDataTool.deletes;

import javax.jcr.Node;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * @author Pavan Ginjupalli Date: 08/12/14
 */
public class ManageNodes {

    private static final Logger log = LoggerFactory.getLogger(ManageNodes.class.getSimpleName());


    public static void DeleteNode(CQDataConfig cfg) throws Exception {   	
    	

        try {        	
        	String queryString = cfg.getQuery();
        	QueryManager queryManager = CQDataConfig.getRootNode().getSession().getWorkspace().getQueryManager();
        	Query query = queryManager.createQuery(queryString,Query.JCR_SQL2);
        	 // Execute the query and get the results ...
            javax.jcr.query.QueryResult result = query.execute();
            RowIterator rows = result.getRows();
            while (rows.hasNext()) {                
                Row row = rows.nextRow();
                Node na = row.getNode();
                na.getPath();
                System.out.println("Deleting the node -->"+  na.getPath());
                na.remove();             
            }

        }
        catch (Exception e)
        {
            log.error(ExceptionMsg.formatExceptionMessage("Run Delete Exception", e));
        }

    }//End DeleteNode Method

}//End class ManageNodes
