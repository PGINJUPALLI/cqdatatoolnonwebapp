package com.sni.home.CQDataTool.utils;

public class CleanString {
	
	 public String clean(String textValue) throws Exception
	    {
		 
		 textValue= textValue.replaceAll("\n", "");
		 textValue= textValue.replaceAll("\t", "");
		 textValue= textValue.replaceAll("\\n", "");
		 textValue= textValue.replaceAll("\\t", "");
		 textValue= textValue.replaceAll("\r", "");
		 textValue= textValue.replaceAll("\\r", "");
		 textValue= textValue.replaceAll("\r\n", "");
		 textValue= textValue.replaceAll("\\r\\n", "");
		 textValue= textValue.replaceAll("[|]",""); //Replacing the pipe symbols
		 
		 return textValue;
	      
	    }

}
