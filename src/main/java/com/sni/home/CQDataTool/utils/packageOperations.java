package com.sni.home.CQDataTool.utils;

/**
 * Created by 161257 on 8/26/2015.
 *  Custom_Gen_Pkg
    pavan
    Reusable pkg which will be modified/built via java program using list of custom filters
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.jackrabbit.vault.packaging.JcrPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.config.CQDataConfig;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import org.apache.jackrabbit.vault.packaging.impl.JcrPackageManagerImpl;

public class packageOperations {

    static Logger log = LoggerFactory.getLogger(packageOperations.class);

    public static void addFilters(CQDataConfig cfg) throws IOException {

        try {

            log.info("The file name is: " + cfg.getInfile());

            FileReader input = new FileReader(cfg.getInfile());

            BufferedReader bufRead = new BufferedReader(input);

            String filter = null;
            String pkgpath = cfg.getPkgpath();
            Integer filternum = 0;
            String[] multirules = new String[4];
            JcrPackageManagerImpl jpkg = new JcrPackageManagerImpl(CQDataConfig.getRootNode().getSession());
            String pkggrp="pavan";
            String pkgname="Custom_Gen_Pkg";
            jpkg.create(pkggrp,pkgname);
            Node FilterParentNode = CQDataConfig.getRootNode().getSession().getNode(pkgpath);

            if (FilterParentNode.hasNode("filter")) {
                Node delnode = CQDataConfig.getRootNode().getSession().getNode(pkgpath + "/filter");
                delnode.remove();
                CQDataConfig.getRootNode().getSession().save();
            }

            Node FilterNode = FilterParentNode.addNode("filter");

            while ((filter = bufRead.readLine()) != null)
            {
                String filtervalue = filter.toString();
                log.info("Adding filter: " + filtervalue);

                try {
                    if (!FilterNode.equals(null)) {

                        FilterNode.addNode("f" + filternum, "nt:unstructured");
                        CQDataConfig.getRootNode().getSession().save();
                        Node filterchild = CQDataConfig.getRootNode().getSession().getNode(pkgpath + "/filter/f"+filternum);
                            filterchild.setProperty("mode", "replace");
                            filterchild.setProperty("root", filtervalue);
                            filterchild.setProperty("rules", multirules);

                    }
                } catch (PathNotFoundException p) {
                    log.info("Not found, Node " + pkgpath);
                }

                filternum = filternum + 1;

            }


            CQDataConfig.getRootNode().getSession().save();
            bufRead.close();
        } catch (Exception e) {
            log.error(ExceptionMsg.formatExceptionMessage("pkgOperations process Exception", e));
        }

    }


}