package com.sni.home.CQDataTool.utils;

import javax.jcr.Value;

public class GetCQPropertyValue {
	 public String getCQPropertyValue(javax.jcr.Property propname) throws Exception
	    {
		 String propvalue="";
		  if (propname.getDefinition().isMultiple()) {
              Value[] values = propname.getValues();
              for (int i = 0; i < values.length; i++) {
                  propvalue = propvalue + values[i].getString()+" "; //Multiple properties seperated by space
              }// End of values for loop
              } else {
              propvalue = propname.getValue().getString();
          }// End if isMultiple
		  return propvalue;
	    }

}
