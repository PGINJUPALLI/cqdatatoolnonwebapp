package com.sni.home.CQDataTool.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.config.CQDataConfig;

public class FileOperations {

static Logger log	= LoggerFactory.getLogger(FileOperations.class);

static FileWriter fw;
	
	public static void appendtoFile(CQDataConfig cfg,ArrayList<String> filecontent) throws IOException 
	{
	
		try {
			
			     fw= new FileWriter(cfg.getOutfile());
			     
				 log.info(cfg.getOutfile());			
				 
				 fw.append(cfg.getColumns()+"\n");
				 
				 for(String data:filecontent)
				 	{
					 //System.out.println("filecontent:"+data);
					 fw.append(data+"\n");					 
					}
					fw.flush();
					fw.close();
			 }

		catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage(
					"File to write not found Exception", e));
			}
		

	}
	

	

}
