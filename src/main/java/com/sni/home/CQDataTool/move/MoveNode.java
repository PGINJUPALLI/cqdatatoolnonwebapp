package com.sni.home.CQDataTool.move;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
//import com.day.cq.wcm.api.PageManager;

/* An example input file content
 * /content/diy-com/en/how-to/outdoors/gardening/photo-galleries/2009/s/spring-colors-for-the-garden|/content/diy-com/en/how-to/outdoors/gardening/photo-galleries/2012/s/spring-colors-for-the-garden
 * 
 * Note: The destination node path should be valid, other wise exception will be thrown
 * example: when moving /abc/123/file1 into /lmn/232/file1 , "/lmn/232" should be a valid path
 * 
 * 
 * Append following to CQDataUpdateFactory
 *        	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("movenode")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   MoveNode.moveNode(cqdataconfobj);
    	   }
 * 
 * 
 * Add following to Beanfile
 *     <bean id="Update" class="com.sni.home.CQDataTool.config.CQDataConfig">
        <property name="infile" value="C:/files/outputfiles/input.txt" />
        <property name="updatetype" value="movenode" />
        </property>
    </bean>
    
  *Program Argument: update
  *Example code reference:
  *https://apache.googlesource.com/jackrabbit/+/61b4cab9ef4ab212dcceab2ccda7c0fb3242f87a/jackrabbit-core/src/main/java/org/apache/jackrabbit/core/BatchedItemOperations.java
  *http://www.day.com/specs/jsr170/javadocs/jcr-2.0/javax/jcr/Workspace.html
  *http://docs.adobe.com/docs/en/cq/5-6-1/javadoc/com/day/cq/wcm/api/PageManager.html#move(com.day.cq.wcm.api.Page, java.lang.String, java.lang.String, boolean, boolean, java.lang.String[])
 */
public class MoveNode {

	private static final Logger log = LoggerFactory.getLogger(MoveNode.class);

	public static void moveNode(CQDataConfig cfg) throws Exception {

		try {
			log.info("The Input update file name is: " + cfg.getInfile());
			FileReader input = new FileReader(cfg.getInfile());
			BufferedReader bufRead = new BufferedReader(input);
			String myLine = null;
			Node cqmoveNode = null;


			while ((myLine = bufRead.readLine()) != null) {
				String[] param = myLine.split(cfg.getDelimit(), -1);

				String cqmoveNodeStr = param[0].toString();
				String cqdestinationNodeStr = param[1].toString();

				try {
					cqmoveNode = CQDataConfig.getRootNode().getSession().getNode(cqmoveNodeStr);
					System.out.println(cqmoveNode.getPath().toString());
					move(cqmoveNode, cqdestinationNodeStr);
				} catch (PathNotFoundException p) {
					log.info("source or destination path doesnt exist" + p);
				} catch (Exception notanode) {
					log.info("Exception is:" + notanode);
					log.info(cqmoveNodeStr + " does not exist..skipping to next");
				}

			}

			bufRead.close();
			log.info("no more nodes found,Process Completed");
		} catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage(
				"Run Update Exception", e));
		}

	}

	public static Node move(Node sourcepath, String destinationpath)
	throws RepositoryException {

		CQDataConfig.getRootNode().getSession().getWorkspace().move(sourcepath.getPath(), destinationpath);
		CQDataConfig.getRootNode().getSession().save();
		return null;
	}
	
	/*public void move2(String srcAbsPath,String destAbsPath) 
	throws RepositoryException {
		  NodeService nodeService=getRepositoryImpl().getServiceRegistry().getNodeService();
		  NodeRef rootRef=nodeService.getRootNode(workspaceStore);
		  NodeRef sourceRef=ItemResolver.getNodeRef(this,rootRef,srcAbsPath);
		  if (sourceRef == null) {
		    throw new PathNotFoundException("Source path " + srcAbsPath + " cannot be found.");
		  }
		  NodeRef destRef=null;
		  QName destName=null;
		  Path destPath=new JCRPath(getNamespaceResolver(),destAbsPath).getPath();
		  if (destPath.size() == 1) {
		    destRef=rootRef;
		    destName=((JCRPath.SimpleElement)destPath.get(0)).getQName();
		  }
		 else {
		    Path destParentPath=destPath.subPath(destPath.size() - 2);
		    destRef=ItemResolver.getNodeRef(this,rootRef,destParentPath.toPrefixString(getNamespaceResolver()));
		    if (destRef == null) {
		      throw new PathNotFoundException("Destination path " + destParentPath + " cannot be found.");
		    }
		    destName=((JCRPath.SimpleElement)destPath.get(destPath.size() - 1)).getQName();
		  }
		  if (destName.getLocalName().indexOf('[') != -1 || destName.getLocalName().indexOf(']') != -1) {
		    throw new RepositoryException("Node name '" + destName + "' is invalid");
		  }
		  ChildAssociationRef childAssocRef=nodeService.getPrimaryParent(sourceRef);
		  nodeService.moveNode(sourceRef,destRef,childAssocRef.getTypeQName(),destName);
		}
*/


}