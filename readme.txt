When first setting up, run the Maven install option to download all dependencies.


The following VM option is required:
-Dmy.env=<ENVIRONMENT>
where <ENVIRONMENT> may be local, dev, or prod

I have a run configuration setup with VM Options:
  -Xmx512m -Dlogback.configurationFile=file:/Users/155305/Workspace/IDEA/cqDataTool/target/classes/logback.xml
this increases the max heap size and provides an override for the provided logback.xml file in
jackrabbit-standalone-2.8.0 to make it log much less.

The logback.xml override does the following:

Creates filters to control what gets logged where.
Filters can be turned on, off, or adjusted as needed
All logging goes to the console.
<Filter> Logging level is set debug and below.
<Filter> Only log messages from com.sni.home packages are written

